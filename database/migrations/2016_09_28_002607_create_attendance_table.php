<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('year');
            $table->integer('month');
            $table->integer('day');
            $table->time('from_time')->nullable();
            $table->time('to_time')->nullable();
            $table->string('comment', 256)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('attendances');
    }
}
