<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/attendance/', 'AttendanceController@index');
Route::get('/attendance/{year}/{month}', 'AttendanceController@indexYM');

Route::get('/attendance_data/{year}/{month}', 'AttendanceController@getData');
Route::get('/attendance_data/{year}/{month}/{day}', 'AttendanceController@getDataOne');

Route::post('/attendance_data/{year}/{month}/{day}', 'AttendanceController@store');

Route::post('/attendance_from/{year}/{month}/{day}', 'AttendanceController@store_from');
Route::post('/attendance_to/{year}/{month}/{day}', 'AttendanceController@store_to');