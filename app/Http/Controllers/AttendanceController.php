<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Log;

/**
 * 出勤のAPIコントローラ
 */
class AttendanceController extends Controller
{
    //

    /**
     *
     */
    public function index(Request $request)
    {
        $year = date('Y');
        $month = date('n');
        return view('attendance', compact("year", "month"));
    }
    public function indexYM(Request $request, int $year, int $month)
    {
        return view('attendance', compact("year", "month"));
    }

    public function getData(Request $request, int $year, int $month)
    {
        $user = Auth::user();

        $week_str_list = array( '日', '月', '火', '水', '木', '金', '土');

        $viewData = [];

        // 翌月最初
        $nextDate = (new \DateTime())->add(new \DateInterval('P1M'));

        // DBからデータを取ってくる
        $_list = \App\AttendanceModel::where('user_id', $user->id)
                ->where('year', $year)
                ->where('month', $month)
                ->orderBy('day', 'asc')
                ->get()
                ->all();

        if(empty($_list) === false) {
            // 中身が配列じゃない(AttendanceModel)のでこれではだめ
            // $_list　= array_column($_list, null, 'day');
            $_wkList = [];
            foreach ($_list as $value) {
                $_wkList[$value->day] = $value;
            }
            $_list = $_wkList;
        }

        // サーバーの日付
        $nowDate = new \DateTime();

        // カレンダーデータを作る
        $date = new \DateTime();
        $date->setDate($year, $month, 1);

        // 月が一緒の間
        while($date->format('m') == $month) {
            $viewData[] = [
                "day" => $date->format('j'),
                "wj" => $week_str_list[$date->format('w')],
                "data" => $_list[$date->format('j')] ?? ["from_time" => "", "to_time" => "", "comment" => ""],
                "now" => $date->format('Ymd') === $nowDate->format('Ymd'),
                "will" => $date->format('Ymd') > $nowDate->format('Ymd'),
            ];
            $date->add(new \DateInterval('P1D'));
        }

        return ["attendances" => $viewData, "year" => $year, "month" => $month];
    }
    public function getDataOne(Request $request, int $year, int $month, int $day)
    {
        // DBからデータを取ってくる
        $_attendance = $this->getAttendanceData($year, $month, $day);

        if(empty($_attendance) !== false) {
            $_attendance = ["from_time" => "", "to_time" => "", "comment" => ""];
        }

        return $_attendance;
    }

    protected function getAttendanceData(int $year, int $month, int $day)
    {
        $user = Auth::user();

        // データを取ってくる
        $_attendance = \App\AttendanceModel::where('user_id', $user->id)
                ->where('year', $year)
                ->where('month', $month)
                ->where('day', $day)
                ->first();
        return($_attendance);
    }

    /**
     * データを作る
     */
    public function store(Request $request, int $year, int $month, int $day)
    {
        $user = Auth::user();

        // データを取ってくる
        $_attendance = $this->getAttendanceData($year, $month, $day);

        if(empty($_attendance) !== false) {
            $_attendance = new \App\AttendanceModel;

            $_attendance->user_id = $user->id;
            $_attendance->year = $year;
            $_attendance->month = $month;
            $_attendance->day = $day;
        }
        $_attendance->from_time = $request->input('from_time') ?: null;
        $_attendance->to_time = $request->input('to_time') ?: null;
        $_attendance->comment = $request->input('comment') ?: null;

        $_attendance->save();

        return($_attendance);
    }

    /**
     * データを作る
     */
    public function store_from(Request $request, int $year, int $month, int $day)
    {
        $user = Auth::user();

        // データを取ってくる
        $_attendance = $this->getAttendanceData($year, $month, $day);

        if(empty($_attendance) !== false) {
            $_attendance = new \App\AttendanceModel;

            $_attendance->user_id = $user->id;
            $_attendance->year = $year;
            $_attendance->month = $month;
            $_attendance->day = $day;
        }
        $_attendance->from_time = (new \DateTime())->format("H:i");

        $_attendance->save();

        return($_attendance);
    }

    /**
     * データを作る
     */
    public function store_to(Request $request, int $year, int $month, int $day)
    {
        $user = Auth::user();

        // データを取ってくる
        $_attendance = $this->getAttendanceData($year, $month, $day);

        if(empty($_attendance) !== false) {
            $_attendance = new \App\AttendanceModel;

            $_attendance->user_id = $user->id;
            $_attendance->year = $year;
            $_attendance->month = $month;
            $_attendance->day = $day;
        }
        $_attendance->to_time = (new \DateTime())->format("H:i");

        $_attendance->save();

        return($_attendance);
    }
}
